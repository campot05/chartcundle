import React from 'react';
import './App.scss';
import Crash from './components/Crash/Crash';

function App() {
  return (
    <div className="App">
      <Crash />
    </div>
  );
}

export default App;
