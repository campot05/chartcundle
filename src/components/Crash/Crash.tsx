import { useEffect, useState } from 'react';
import CrashCanvas from '../CrashCanvas/CrashCanvas';
import './Crash.scss';

export interface Candlestick {
  o: number;
  l: number;
  c: number;
  x: number;
  h: number;
  s: [number, number];
}

const Crash = () => {
  const [candlesticks, setCandlesticks] = useState<Candlestick[]>([]);
  const [intervalId, setIntervalId] = useState<NodeJS.Timeout | null>(null);
  const [isGenerate, setisGenerate] = useState(false);

  //   геренация свечей
  useEffect(() => {
    if (isGenerate && !intervalId) {
      setCandlesticks((prev) => [
        ...prev,
        {
          o: 0,
          l: 0,
          c: 0,
          x: 0,
          h: 0,
          s: [0, 1],
        },
      ]);
      const id = setInterval(() => {
        const s0 = Math.random() * (100 - 1) + 1;
        const s1 = Math.random() * (100 - 1) + 1;
        const l = s0 - 5;
        const h = s1 + 5;

        const newCandlestick: Candlestick = {
          o: 1,
          l,
          c: 1,
          x: 1,
          h,
          s: [parseFloat(s0.toFixed(2)), parseFloat(s1.toFixed(2))], // массив с ценами
        };

        setCandlesticks((prevCandlesticks) => {
          const newCandlesticks = [...prevCandlesticks, newCandlestick];
          if (newCandlesticks.length > 13) {
            return newCandlesticks.slice(newCandlesticks.length - 13);
          }
          return newCandlesticks;
        });
      }, 1000);

      setIntervalId(id);
    }

    return () => {
      if (intervalId) {
        clearInterval(intervalId);

        setIntervalId(null);
      }
    };
  }, [isGenerate, intervalId, setCandlesticks]);

  return (
    <div className="wrap">
      <div className="crash__game">
        <div className="crash__game-row">
          <CrashCanvas candleStiks={candlesticks} userBet={50} />
        </div>
      </div>
      <button onClick={() => setisGenerate((prev) => !prev)}>
        {isGenerate ? 'stop generate' : 'start generate'}
      </button>
    </div>
  );
};

export default Crash;
