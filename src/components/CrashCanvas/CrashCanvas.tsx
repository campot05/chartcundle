import { useEffect, useRef, useState } from 'react';
import { Candlestick } from '../Crash/Crash';
import Chart, { ChartType } from 'chart.js/auto';

interface CandlestickPluginOptions {
  crashed: boolean;
}

declare module 'chart.js' {
  interface PluginOptionsByType<TType extends ChartType> {
    candlestick?: CandlestickPluginOptions;
    currentCrash?: CandlestickPluginOptions;
  }
}

type Props = {
  userBet: number;
  candleStiks: Candlestick[];
};

const CrashCanvas: React.FC<Props> = ({ userBet = 50, candleStiks }) => {
  const chartRef = useRef<HTMLCanvasElement>(null);

  const [lastCandleIndex, setLastCandleIndex] = useState<number>(-1);

  useEffect(() => {
    if (candleStiks.length > 0) {
      setLastCandleIndex(candleStiks.length - 1);
    }
  }, [candleStiks]);

  useEffect(() => {
    if (chartRef.current && lastCandleIndex !== -1) {
      if (chartRef.current) {
        const ctx = chartRef.current.getContext('2d');

        if (ctx) {
          const browserWidth = window.innerWidth;
          let thickness = 13;
          let topPadding = 30;

          if (browserWidth < 600) {
            thickness = 7;
            topPadding = 15;
          }

          const candlestick = {
            id: 'candlestick',
            // @ts-ignore
            beforeDatasetsDraw(chart, args, options) {
              const {
                ctx,
                data,
                scales: { y },
              } = chart;

              ctx.save();
              ctx.lineWidth = 2;
              ctx.setLineDash([10, 0]);
              // @ts-ignore

              data.datasets[0].data.forEach((datapoint, index) => {
                ctx.beginPath();
                ctx.moveTo(
                  chart.getDatasetMeta(0).data[index].x,
                  chart.getDatasetMeta(0).data[index].y
                );
                ctx.lineTo(
                  chart.getDatasetMeta(0).data[index].x,
                  y.getPixelForValue(data.datasets[0].data[index].h)
                );
                ctx.stroke();
                ctx.strokeStyle = 'rgba(126, 199, 108, 1)';

                if (
                  options.crashed &&
                  index === data.datasets[0].data.length - 1
                ) {
                  ctx.strokeStyle = 'rgba(126, 199, 108, 1)';
                }

                ctx.beginPath();
                ctx.moveTo(
                  chart.getDatasetMeta(0).data[index].x,
                  chart.getDatasetMeta(0).data[index].y
                );
                ctx.lineTo(
                  chart.getDatasetMeta(0).data[index].x,
                  y.getPixelForValue(data.datasets[0].data[index].l)
                );
                ctx.stroke();
              });
            },
          };

          const currentCrash = {
            id: 'currentCrash',
            //  @ts-ignore
            beforeDatasetsDraw(chart, args, options) {
              const {
                ctx,
                data,
                chartArea: { top, left, width },
                scales: { y },
              } = chart;

              ctx.save();
              ctx.font = 'bold 27px Montserrat';

              if (options.crashed) {
                ctx.fillStyle = '#7EC76C';
              }

              ctx.fillText(
                `x3.5`,
                chart.getDatasetMeta(0).data.at(-1).x + 30,
                chart.getDatasetMeta(0).data.at(-1).y
              );
            },
          };

          const chart = new Chart(ctx, {
            type: 'bar',
            data: {
              labels: [
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
                '10',
                '11',
                '12',
                '13',
                '14',
                '15',
                '16',
                '17',
                '18',
                '19',
                '20',
                '21',
                '22',
                '23',
                '24',
                '25',
                '26',
                '27',
                '28',
                '29',
                '30',
              ],
              datasets: [
                {
                  label: 'Crash Data',
                  data: [...candleStiks],
                  borderColor: '#232734',
                  borderRadius: 3,
                  borderSkipped: false,
                  backgroundColor: ['#7EC76C'],
                  animation: {
                    // анимация для последней свечи на графике
                    duration(ctx, options) {
                      console.log(options);
                      return ctx.dataIndex === candleStiks.length - 1 ? 500 : 0;
                    },
                  },

                  barThickness: thickness,
                },
              ],
            },
            options: {
              layout: {
                padding: {
                  left: 15,
                  top: topPadding,
                },
              },

              parsing: {
                xAxisKey: 'x',
                yAxisKey: 's',
              },

              scales: {
                x: {
                  display: false,
                  grid: {
                    display: false,
                  },
                  max: 1000,
                },
                y: {
                  ticks: {
                    color: '#828695',
                    callback: function (value) {
                      return 'X' + value;
                    },
                  },
                  grid: {
                    color: 'rgba(118, 123, 152, .05)',
                  },
                  border: {
                    display: false,
                  },

                  // max: multiplier < 3 ? 3 : multiplier,
                },
              },

              plugins: {
                tooltip: {
                  enabled: false,
                },
                legend: {
                  display: false,
                },
                candlestick: {
                  crashed: true,
                },
                currentCrash: {
                  crashed: true,
                },
              },
            },
            plugins: [currentCrash, candlestick],
          });

          Chart.defaults.font.family = 'Montserrat';
          Chart.defaults.font.size = 9;
          Chart.defaults.font.weight = 500;

          return () => {
            chart.destroy();
          };
        }
      }
    }
  }, [userBet, candleStiks]);

  return <canvas ref={chartRef} />;
};

export default CrashCanvas;
